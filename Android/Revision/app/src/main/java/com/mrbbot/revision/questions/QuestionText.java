package com.mrbbot.revision.questions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mrbbot.revision.R;

class QuestionText extends Question {
    private final String question;
    private final String answer;

    QuestionText(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    @Override
    protected View inflate(LayoutInflater inflater, final ViewGroup container) {
        final View root = inflater.inflate(R.layout.question_type_text, container, false);

        TextView questionTextView = (TextView) root.findViewById(R.id.question_type_text_question);
        questionTextView.setText(question);

        final EditText questionEditText = (EditText) root.findViewById(R.id.question_type_text_answer);

        final Question questionObject = this;
        Button questionButton = (Button) root.findViewById(R.id.question_type_text_button);
        questionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionManager.nextQuestion(questionObject, container, root, questionEditText.getText().toString());
            }
        });

        return root;
    }

    @Override
    protected boolean checkAnswer(View root, Object... sendAnswers) {
        return answer.toLowerCase().equals(((String)sendAnswers[0]).toLowerCase());
    }
}
