package com.mrbbot.revision;

import android.support.v4.view.ViewPager;
import android.view.View;

public class FadePageTransformer implements ViewPager.PageTransformer {
    @Override
    public void transformPage(View page, float position) {
        if (position < -1) {
            page.setAlpha(0);
        } else if (position < 0) {
            page.setAlpha(Math.abs(position));
        } else if (position == 0) {
            page.setAlpha(1);
        } if (position <= 1) {
            page.setAlpha(1 - Math.abs(position));
        } else {
            page.setAlpha(0);
        }
    }
}