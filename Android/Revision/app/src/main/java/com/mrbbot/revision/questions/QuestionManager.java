package com.mrbbot.revision.questions;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mrbbot.revision.CategoryActivity;
import com.mrbbot.revision.MainActivity;
import com.mrbbot.revision.R;

import java.util.ArrayList;

public class QuestionManager {
    public static ArrayList<Category> questions;

    static final int COLOUR_CORRECT = Color.argb(255, 0, 200, 0);
    static final int COLOUR_INCORRECT = Color.argb(255, 255, 0, 0);

    private static String[] getStringArrayFromData(DataSnapshot data) {
        String[] array = new String[Integer.parseInt(String.valueOf(data.getChildrenCount()))];
        int index = 0;
        for(DataSnapshot choice : data.getChildren()) {
            array[index] = (String) choice.getValue();
            index++;
        }
        return array;
    }

    private static boolean[] getBooleanArrayFromData(DataSnapshot data) {
        boolean[] array = new boolean[Integer.parseInt(String.valueOf(data.getChildrenCount()))];
        int index = 0;
        for(DataSnapshot choice : data.getChildren()) {
            array[index] = (boolean) choice.getValue();
            index++;
        }
        return array;
    }

    private static Question getQuestionFromData(DataSnapshot data) {
        Question questionObject = null;
        switch(data.child("type").getValue(Integer.class)) {
            case 0: //Text
                questionObject = new QuestionText(
                        (String) data.child("question").getValue(),
                        (String) data.child("answer").getValue()
                );
                break;
            case 1: //Multiple Choice
                questionObject = new QuestionMultipleChoice(
                        (String) data.child("question").getValue(),
                        getStringArrayFromData(data.child("choices")),
                        data.child("correct").getValue(Integer.class)
                );
                break;
            case 2: //True/False
                questionObject = new QuestionTrueFalse(
                        (String) data.child("question").getValue(),
                        (Boolean) data.child("answer").getValue()
                );
                break;
            case 3: //Sort List
                questionObject = new QuestionSortList(
                        (String) data.child("question").getValue(),
                        getStringArrayFromData(data.child("items"))
                );
                break;
            case 4: //Match Items
                questionObject = new QuestionMatchItems(
                        (String) data.child("question").getValue(),
                        getStringArrayFromData(data.child("items"))
                );
                break;
            case 5: //Tick All
                questionObject = new QuestionTickAll(
                        (String) data.child("question").getValue(),
                        getStringArrayFromData(data.child("items")),
                        getBooleanArrayFromData(data.child("correct"))
                );
                break;
            case 6: //Card
                questionObject = new QuestionCard(
                        data.child("front").getValue(),
                        data.child("back").getValue()
                );
                break;

        }
        return questionObject;
    }

    public static void initQuestions() {
        questions = new ArrayList<>();
        for(Categories category : Categories.values())
            questions.add(new Category(category));

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference questionPacks = database.getReference("question-packs");
        questionPacks.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot pack : dataSnapshot.getChildren()) {
                    String name = (String) pack.child("name").getValue();
                    String description = (String) pack.child("description").getValue();
                    Categories category = Categories.fromID(pack.child("category").getValue(Integer.class));

                    ArrayList<Question> questions = new ArrayList<>();
                    for(DataSnapshot question : pack.child("questions").getChildren()) {
                        Question questionObject = getQuestionFromData(question);
                        if(questionObject != null)
                            questions.add(questionObject);
                    }

                    QuestionPack questionPack = new QuestionPack(name, description, questions, category);
                    addQuestionPack(questionPack);
                }

                if(MainActivity.INSTANCE != null)
                    MainActivity.INSTANCE.notifyQuestionDatasetChanged();
                if(CategoryActivity.INSTANCE != null)
                    CategoryActivity.INSTANCE.notifyQuestionDatasetChanged();

                Log.i("QuestionLoader", "Loaded questions");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("QuestionLoader", "Failed to load questions!");
            }
        });

        ArrayList<Question> testPackQuestions = new ArrayList<>();
        testPackQuestions.add(new QuestionCard("<u>Biology</u> Banner", R.drawable.biology_banner));
        testPackQuestions.add(new QuestionText("What is my favourite colour?", "Blue"));
        testPackQuestions.add(new QuestionTrueFalse("This is an app.", true));
        testPackQuestions.add(new QuestionMultipleChoice("What is the colour of a Physics Book?", new String[] {"Red", "Yellow", "Blue", "Purple"}, 1));
        testPackQuestions.add(new QuestionSortList("Sort this list:", new String[] {"I go first...", "...I'm second...", "...me third...", "...and I'm last :(."}));
        testPackQuestions.add(new QuestionMatchItems("Match these items:", new String[] {"Thing 1", "Answer 1", "Thing 2", "Answer 2", "Thing 3", "Answer 3", "Thing 4", "Answer 4", "Thing 5", "Answer 5"}));
        testPackQuestions.add(new QuestionTickAll("Tick all that apply:", new String[] {"Yes ME Please 1", "Nah, not me 2", "Please tick me :D :) 3", "I shouldn't be ticked 4"}, new boolean[] {true, false, true, false}));
        QuestionPack testPack = new QuestionPack("Test Question Pack", "This is a pack for testing new question types.", testPackQuestions, Categories.MISC);
        addQuestionPack(testPack);

        if(MainActivity.INSTANCE != null)
            MainActivity.INSTANCE.notifyQuestionDatasetChanged();
        if(CategoryActivity.INSTANCE != null)
            CategoryActivity.INSTANCE.notifyQuestionDatasetChanged();
    }

    private static void addQuestionPack(QuestionPack pack) {
        for(Category category : questions) {
            if(category.getCategory() == pack.getCategory()) {
                category.getQuestionPacks().add(pack);
            }
        }
    }


    static void nextQuestion(Question question, ViewGroup container, View root, Object... answers) {
        if(question.checkAnswer(root, answers)) {
            Toast.makeText(container.getContext(), "Correct!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(container.getContext(), "Incorrect!", Toast.LENGTH_SHORT).show();
        }

        /*final ViewPager pager = (ViewPager) container;
        if(pager.getCurrentItem() < pager.getAdapter().getCount() - 1) {
            pager.postDelayed(new Runnable() {
                @Override
                public void run() {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                }
            }, 1000);
        }*/
    }
}
