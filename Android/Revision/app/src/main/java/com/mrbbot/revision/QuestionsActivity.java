package com.mrbbot.revision;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.ProgressBar;

import com.mrbbot.revision.questions.QuestionFragment;
import com.mrbbot.revision.questions.QuestionManager;

public class QuestionsActivity extends AppCompatActivity {
    private int categoryIndex;
    private int packIndex;
    private ProgressBar pagerProgress;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new Slide(Gravity.BOTTOM));
        }

        categoryIndex = getIntent().getIntExtra("category-index", 0);
        packIndex = getIntent().getIntExtra("pack-index", 0);
        setContentView(R.layout.activity_questions);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(packIndex).getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        pagerProgress = (ProgressBar) findViewById(R.id.pager_progress);
        pagerProgress.setProgressDrawable(ContextCompat.getDrawable(this, R.drawable.progress_indicator));

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new QuestionPagerAdapter(getSupportFragmentManager()));
        pager.setPageTransformer(false, new FadePageTransformer());
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((QuestionsActivity)pager.getContext()).onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        onPageSelected(0);
    }

    private void onPageSelected(int position) {
        float section = 100f / (float)pager.getAdapter().getCount();

        ProgressBarAnimation animation = new ProgressBarAnimation(pagerProgress, section, position);
        animation.setDuration(250);
        animation.setInterpolator(new DecelerateInterpolator());
        pagerProgress.startAnimation(animation);
    }

    private class ProgressBarAnimation extends Animation {
        private ProgressBar progressBar;
        private float from, secondaryFrom;
        private float to, secondaryTo;

        ProgressBarAnimation(ProgressBar progressBar, float section, int position) {
            super();
            this.progressBar = progressBar;
            this.from = progressBar.getProgress();
            this.secondaryFrom = progressBar.getSecondaryProgress();
            this.to = section * position;
            this.secondaryTo = section * (position + 1);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            float value = from + (to - from) * interpolatedTime;
            float secondaryValue = secondaryFrom + (secondaryTo - secondaryFrom) * interpolatedTime;
            progressBar.setProgress((int) value);
            progressBar.setSecondaryProgress((int) secondaryValue);
        }
    }

    @Override
    public void onBackPressed() {
        if(pager.getCurrentItem() > 0) {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        } else {
            super.onBackPressed();
        }
    }

    private class QuestionPagerAdapter extends FragmentStatePagerAdapter {

        QuestionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            QuestionFragment fragment = new QuestionFragment();

            Bundle bundle = new Bundle();

            bundle.putInt("category-index", categoryIndex);
            bundle.putInt("pack-index", packIndex);
            bundle.putInt("question-index", position);

            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public int getCount() {
            return QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(packIndex).getQuestions().size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAfterTransition();
            } else {
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

}
