package com.mrbbot.revision.questions;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import com.mrbbot.revision.QuestionsActivity;

import java.util.HashMap;
import java.util.Map;

public class MatchItemsLineView extends View {
    private QuestionMatchItems.AllItemsMatchedListener allItemsMatchedListener;
    private QuestionMatchItems question;

    private HashMap<Button, Button> matches;
    private Button itemButton, answerButton;
    private Paint paint, correctPaint, incorrectPaint;

    private int[] rootLocation, viewLocation;
    private Rect itemRect, answerRect;
    private Point startPoint, endPoint, screenSize;

    private HashMap<Button, Integer> colours;

    public MatchItemsLineView(Context context) {
        super(context);
        init();
    }

    public MatchItemsLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MatchItemsLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setAllItemsMatchedListener(QuestionMatchItems.AllItemsMatchedListener allItemsMatchedListener, QuestionMatchItems question) {
        this.allItemsMatchedListener = allItemsMatchedListener;
        this.question = question;
    }

    private void init() {
        matches = new HashMap<>();

        paint = new Paint();
        paint.setColor(Color.parseColor("#3D5AFE"));
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(10f);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        correctPaint = new Paint(paint);
        correctPaint.setColor(QuestionManager.COLOUR_CORRECT);

        incorrectPaint = new Paint(paint);
        incorrectPaint.setColor(QuestionManager.COLOUR_INCORRECT);

        rootLocation = new int[2];
        viewLocation = new int[2];

        itemRect = new Rect();
        answerRect = new Rect();
        startPoint = new Point();
        endPoint = new Point();
        screenSize = new Point();
    }

    @Override
    public void invalidate() {
        super.invalidate();

        if(matches.size() == (question.answers.length / 2))
            allItemsMatchedListener.allItemsMatched();
        else
            allItemsMatchedListener.allItemsNotMatched();
    }

    public HashMap<Button, Button> getMatches() {
        return new HashMap<>(matches);
    }

    public void resetMatches() {
        matches.clear();
    }

    public void forceInvalidate(HashMap<Button, Integer> colours) {
        this.colours = colours;
        invalidate();
        allItemsMatchedListener.allItemsNotMatched();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        getRootView().getLocationOnScreen(rootLocation);

        for(Map.Entry<Button, Button> entry : matches.entrySet()) {
            Button itemButton = entry.getKey();
            Button answerButton = entry.getValue();

            getRelativeRectangle(itemButton, itemRect);
            getRelativeRectangle(answerButton, answerRect);

            startPoint.set(itemRect.centerX() + (itemRect.width() / 2), itemRect.centerY());
            endPoint.set(answerRect.centerX() - (answerRect.width() / 2), answerRect.centerY());

            canvas.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y,
                    (colours != null) ? (colours.get(itemButton) == QuestionManager.COLOUR_CORRECT ? correctPaint : incorrectPaint) : paint);
        }
    }

    private void getRelativeRectangle(View view, Rect out) {
        ((QuestionsActivity) view.getContext()).getWindowManager().getDefaultDisplay().getSize(screenSize);
        view.getLocationOnScreen(viewLocation);
        out.set(viewLocation[0] - rootLocation[0],
                viewLocation[1] - rootLocation[1] - (screenSize.y - getHeight()),
                viewLocation[0] - rootLocation[0] + view.getWidth(),
                viewLocation[1] - rootLocation[1] - (screenSize.y - getHeight()) + view.getHeight());
    }

    public boolean isItemSelecting() {
        return itemButton != null;
    }

    public void setItemButton(Button itemButton) {
        this.itemButton = itemButton;
        checkItemAnswerMatch(itemButton);
    }

    public boolean isAnswerSelecting() {
        return answerButton != null;
    }

    public void setAnswerButton(Button answerButton) {
        this.answerButton = answerButton;
        checkAnswerItemMatch(answerButton);
    }

    private void checkItemAnswerMatch(Button itemButton) {
        if(matches.containsKey(itemButton)) {
            matches.remove(itemButton);
            invalidate();
        }
    }

    private void checkAnswerItemMatch(Button answerButton) {
        if(matches.containsValue(answerButton)) {
            for(Map.Entry<Button, Button> entry : matches.entrySet()) {
                if(entry.getValue().equals(answerButton)) {
                    matches.remove(entry.getKey());
                    break;
                }
            }
            invalidate();
        }
    }

    public void setItemAnswerMatch(Button answerButton) {
        checkAnswerItemMatch(answerButton);
        matches.put(itemButton, answerButton);
        invalidate();
    }

    public void setAnswerItemMatch(Button itemButton) {
        checkItemAnswerMatch(itemButton);
        matches.put(itemButton, answerButton);
        invalidate();
    }

}