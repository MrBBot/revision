package com.mrbbot.revision.questions;

import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Space;
import android.widget.TextView;

import com.mrbbot.revision.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class QuestionMatchItems extends Question {
    private final String question;
    final String[] answers;
    private ArrayList<String> answerList;

    QuestionMatchItems(String question, String[] answers) {
        this.question = question;
        this.answers = answers;
    }

    @Override
    protected void init() {
        answerList = new ArrayList<>();
        for(int i = 1; i < answers.length; i += 2) {
            answerList.add(answers[i]);
        }
        Collections.shuffle(answerList);
    }

    @Override
    protected View inflate(LayoutInflater inflater, final ViewGroup container) {
        final View root = inflater.inflate(R.layout.question_type_match_items, container, false);

        TextView questionTextView = (TextView) root.findViewById(R.id.question_type_match_items_question);
        questionTextView.setText(question);

        final MatchItemsLineView lineView = (MatchItemsLineView) root.findViewById(R.id.question_type_match_items_line_view);

        GridLayout answersLayout = (GridLayout) root.findViewById(R.id.question_type_match_items_answers);
        answersLayout.setRowCount(answers.length / 2);
        answersLayout.setColumnCount(3);

        final ArrayList<Button> itemButtons = new ArrayList<>();
        final ArrayList<Button> answerButtons = new ArrayList<>();

        for(int i = 0; i < answers.length; i += 2) {
            String item = answers[i];
            String answer = answerList.get(i / 2);

            final Button itemButton = new Button(inflater.getContext());
            itemButton.setText(item);
            itemButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(lineView.isItemSelecting()) {
                        for (Button button : itemButtons) {
                            button.setEnabled(true);
                        }
                        lineView.setItemButton(null);
                    } else if(lineView.isAnswerSelecting()) {
                        lineView.setAnswerItemMatch(itemButton);
                        lineView.setAnswerButton(null);
                        for (Button button : answerButtons) {
                            button.setEnabled(true);
                        }
                    } else if(!lineView.isItemSelecting()) {
                        lineView.setItemButton(itemButton);
                        for (Button button : itemButtons) {
                            if (!button.equals(itemButton))
                                button.setEnabled(false);
                        }
                    }
                }
            });
            itemButtons.add(itemButton);

            Space space = new Space(inflater.getContext());

            final Button answerButton = new Button(inflater.getContext());
            answerButton.setText(answer);
            answerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(lineView.isAnswerSelecting()) {
                        for (Button button : answerButtons) {
                            button.setEnabled(true);
                        }
                        lineView.setAnswerButton(null);
                    } else if(lineView.isItemSelecting()) {
                        lineView.setItemAnswerMatch(answerButton);
                        lineView.setItemButton(null);
                        for (Button button : itemButtons) {
                            button.setEnabled(true);
                        }
                    } else if(!lineView.isAnswerSelecting()) {
                        lineView.setAnswerButton(answerButton);
                        for (Button button : answerButtons) {
                            if (!button.equals(answerButton))
                                button.setEnabled(false);
                        }
                    }
                }
            });
            answerButtons.add(answerButton);

            answersLayout.addView(itemButton, new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f)));
            answersLayout.addView(space, new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f)));
            answersLayout.addView(answerButton, new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f)));
        }

        final Button button = (Button) root.findViewById(R.id.question_type_match_items_button);
        button.setEnabled(false);
        lineView.setAllItemsMatchedListener(new AllItemsMatchedListener() {
            @Override
            public void allItemsMatched() {
                button.setEnabled(true);
            }

            @Override
            public void allItemsNotMatched() {
                button.setEnabled(false);
            }
        }, this);
        final Question questionObject = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionManager.nextQuestion(questionObject, container, root);
            }
        });

        return root;
    }

    @Override
    protected boolean checkAnswer(View root, Object... sentAnswers) {
        GridLayout answersLayout = (GridLayout) root.findViewById(R.id.question_type_match_items_answers);
        answersLayout.removeAllViews();

        MatchItemsLineView lineView = (MatchItemsLineView) root.findViewById(R.id.question_type_match_items_line_view);
        HashMap<Button, Button> givenMatches = lineView.getMatches();
        lineView.resetMatches();

        HashMap<Button, Integer> colours = new HashMap<>();

        boolean correct = true;

        for(int i = 0; i < answers.length; i += 2) {
            String item = answers[i];
            String answer = answers[i + 1];

            final Button itemButton = new Button(root.getContext());
            itemButton.setText(item);

            Space space = new Space(root.getContext());

            final Button answerButton = new Button(root.getContext());
            answerButton.setText(answer);

            boolean itemCorrect = checkMatch(givenMatches, item, answer);
            colours.put(itemButton, itemCorrect ? QuestionManager.COLOUR_CORRECT : QuestionManager.COLOUR_INCORRECT);
            if(!itemCorrect)
                correct = false;

            answersLayout.addView(itemButton, new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f)));
            answersLayout.addView(space, new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f)));
            answersLayout.addView(answerButton, new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f)));

            lineView.setItemButton(itemButton);
            lineView.setItemAnswerMatch(answerButton);
            lineView.setItemButton(null);
        }

        lineView.forceInvalidate(colours);

        return correct;
    }

    private boolean checkMatch(HashMap<Button, Button> givenMatches, String item, String answer) {
        for(Map.Entry<Button, Button> entry : givenMatches.entrySet()) {
            if(entry.getKey().getText().toString().equals(item)) {
                return entry.getValue().getText().toString().equals(answer);
            }
        }
        return false;
    }

    interface AllItemsMatchedListener {
        void allItemsMatched();
        void allItemsNotMatched();
    }
}
