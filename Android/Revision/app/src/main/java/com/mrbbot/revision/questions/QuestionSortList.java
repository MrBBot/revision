package com.mrbbot.revision.questions;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrbbot.revision.R;

import java.util.ArrayList;
import java.util.Collections;

class QuestionSortList extends Question {
    private final String question;
    private String[] answers;
    private ArrayList<String> answersList;
    private ItemTouchHelper touchHelper;

    QuestionSortList(String question, String[] answers) {
        this.question = question;
        this.answers = answers;
    }

    @Override
    protected void init() {
        answersList = new ArrayList<>();
        for(Object answer : answers)
            answersList.add((String) answer);
        Collections.shuffle(answersList);
    }

    @Override
    protected View inflate(LayoutInflater inflater, final ViewGroup container) {
        final View root = inflater.inflate(R.layout.question_type_sort_list, container, false);

        TextView questionTextView = (TextView) root.findViewById(R.id.question_type_sort_list_question);
        questionTextView.setText(question);

        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.question_type_sort_list_answers);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(inflater.getContext()));

        final Button button = (Button) root.findViewById(R.id.question_type_sort_list_button);
        final Question questionObject = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionManager.nextQuestion(questionObject, container, root);
            }
        });

        QuestionSortListAdapter adapter = new QuestionSortListAdapter(answersList, new OnStartDragListener() {
            @Override
            public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
                if(button.isEnabled())
                    touchHelper.startDrag(viewHolder);
            }
        });
        recyclerView.setAdapter(adapter);

        touchHelper = new ItemTouchHelper(new QuestionTypeSortListItemTouchHelper(adapter, button));
        touchHelper.attachToRecyclerView(recyclerView);

        return root;
    }

    private interface ItemTouchHelperAdapter {
        boolean onItemMove(int fromPosition, int toPosition);
    }

    private interface OnStartDragListener {
        void onStartDrag(RecyclerView.ViewHolder viewHolder);
    }

    private class QuestionSortListAdapter extends RecyclerView.Adapter<QuestionSortListAdapter.ItemViewHolder> implements ItemTouchHelperAdapter {
        class ItemViewHolder extends RecyclerView.ViewHolder {
            final TextView textView;
            final ImageView handleView;
            ItemViewHolder(View view) {
                super(view);
                this.textView = (TextView) view.findViewById(R.id.question_type_sort_list_item_text);
                this.handleView = (ImageView) view.findViewById(R.id.question_type_sort_list_item_handle);
            }
        }

        private ArrayList<String> dataset;
        private ArrayList<Integer> colours;
        private final OnStartDragListener dragStartListener;
        QuestionSortListAdapter(ArrayList<String> dataset, OnStartDragListener dragStartListener) {
            this.dataset = dataset;
            this.dragStartListener = dragStartListener;
        }

        @Override
        public QuestionSortListAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new QuestionSortListAdapter.ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.question_type_sort_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(final QuestionSortListAdapter.ItemViewHolder holder, int position) {
            holder.textView.setText(dataset.get(position));
            if(colours != null)
                holder.textView.setTextColor(colours.get(position));
            if(colours == null) {
                holder.handleView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                            dragStartListener.onStartDrag(holder);
                        }
                        return false;
                    }
                });
            } else {
                holder.handleView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return dataset.size();
        }

        @Override
        public boolean onItemMove(int fromPosition, int toPosition) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(dataset, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(dataset, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        void setDataset(ArrayList<String> dataset) {
            this.dataset = dataset;
        }

        ArrayList<String> getDataset() {
            return dataset;
        }

        void setColours(ArrayList<Integer> colours) {
            this.colours = colours;
        }
    }

    private class QuestionTypeSortListItemTouchHelper extends ItemTouchHelper.Callback {
        private final ItemTouchHelperAdapter adapter;
        private final View button;

        QuestionTypeSortListItemTouchHelper(ItemTouchHelperAdapter adapter, View button) {
            this.adapter = adapter;
            this.button = button;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeMovementFlags(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return button.isEnabled() && adapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

        }

        @Override
        public boolean isLongPressDragEnabled() {
            return button.isEnabled();
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return false;
        }
    }

    @Override
    protected boolean checkAnswer(View root, Object... sentAnswers) {
        RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.question_type_sort_list_answers);
        QuestionSortListAdapter adapter = (QuestionSortListAdapter) recyclerView.getAdapter();
        ArrayList<String> givenAnswers = adapter.getDataset();

        boolean correct = true;

        ArrayList<Integer> colours = new ArrayList<>();
        for(int i = 0; i < answers.length; i++) {
            if(answers[i].equals(givenAnswers.get(i))) {
                colours.add(QuestionManager.COLOUR_CORRECT);
            } else {
                colours.add(QuestionManager.COLOUR_INCORRECT);
                correct = false;
            }
        }

        ArrayList<String> correctAnswers = new ArrayList<>();
        for(Object answer : answers)
            correctAnswers.add((String) answer);
        adapter.setColours(colours);
        adapter.setDataset(correctAnswers);
        adapter.notifyDataSetChanged();
        recyclerView.setEnabled(false);

        root.findViewById(R.id.question_type_sort_list_button).setEnabled(false);

        return correct;
    }
}
