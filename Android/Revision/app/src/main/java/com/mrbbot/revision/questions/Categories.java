package com.mrbbot.revision.questions;

import com.mrbbot.revision.R;

enum Categories {
    PHYSICS ("Physics", R.drawable.misc_banner),            //0
    MATHS ("Maths", R.drawable.misc_banner),                //1
    BIOLOGY ("Biology", R.drawable.biology_banner),         //2
    CHEMISTRY ("Chemistry", R.drawable.chemistry_banner),   //3
    GEOGRAPHY ("Geography", R.drawable.misc_banner),        //4
    HISTORY ("History", R.drawable.misc_banner),            //5
    COMPUTING ("Computing", R.drawable.misc_banner),        //6
    ENGLISH ("English", R.drawable.misc_banner),            //7
    LANGUAGES ("Languages", R.drawable.misc_banner),        //8
    MISC ("Miscellaneous", R.drawable.misc_banner),;        //9

    String name;
    int drawableID;

    Categories(String name, int drawableID) {
        this.name = name;
        this.drawableID = drawableID;
    }

    static Categories fromID(int id) {
        switch (id) {
            case 0:
                return PHYSICS;
            case 1:
                return MATHS;
            case 2:
                return BIOLOGY;
            case 3:
                return CHEMISTRY;
            case 4:
                return GEOGRAPHY;
            case 5:
                return HISTORY;
            case 6:
                return COMPUTING;
            case 7:
                return ENGLISH;
            case 8:
                return LANGUAGES;
            case 9:
                return MISC;
        }
        return MISC;
    }
}
