package com.mrbbot.revision;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.ChangeImageTransform;
import android.transition.Explode;
import android.transition.Slide;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrbbot.revision.questions.QuestionManager;

public class CategoryActivity extends AppCompatActivity {
    public static CategoryActivity INSTANCE;

    private CategoryAdaptor categoryAdaptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        INSTANCE = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            getWindow().setEnterTransition(new Slide(Gravity.BOTTOM));
            getWindow().setExitTransition(new Explode());
            getWindow().setSharedElementEnterTransition(new ChangeImageTransform());
            getWindow().setSharedElementExitTransition(new ChangeImageTransform());
        }

        int categoryIndex = getIntent().getIntExtra("category-index", 0);
        setContentView(R.layout.activity_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(QuestionManager.questions.get(categoryIndex).getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ImageView imageView = (ImageView) findViewById(R.id.banner);
        imageView.setImageDrawable(QuestionManager.questions.get(categoryIndex).getDrawable(this));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.category_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(categoryAdaptor = new CategoryAdaptor(categoryIndex));

        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics())));
    }

    public void notifyQuestionDatasetChanged() {
        categoryAdaptor.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAfterTransition();
            } else {
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private class CategoryAdaptor extends RecyclerView.Adapter<CategoryAdaptor.ViewHolder> {
        private int categoryIndex;

        class ViewHolder extends RecyclerView.ViewHolder {
            private CardView cardView;
            private TextView nameView, descriptionView, questionsView;

            ViewHolder(View itemView) {
                super(itemView);
                this.cardView = (CardView) itemView.findViewById(R.id.category_list_item_card_view);
                this.nameView = (TextView) itemView.findViewById(R.id.category_list_item_name);
                this.descriptionView = (TextView) itemView.findViewById(R.id.category_list_item_description);
                this.questionsView = (TextView) itemView.findViewById(R.id.category_list_item_questions);
            }
        }

        CategoryAdaptor(int categoryIndex) {
            this.categoryIndex = categoryIndex;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final int finalPosition = position;
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), QuestionsActivity.class);
                    intent.putExtra("category-index", categoryIndex);
                    intent.putExtra("pack-index", finalPosition);
                    QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(finalPosition).shuffle();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity) v.getContext()).toBundle());
                    } else {
                        startActivity(intent);
                    }
                }
            });
            holder.nameView.setText(QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(position).getName());
            holder.descriptionView.setText(QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(position).getDescription());
            int numberOfQuestions = QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(position).getQuestions().size();
            holder.questionsView.setText(numberOfQuestions + " Question" + (numberOfQuestions != 1 ? "s" : ""));
        }

        @Override
        public int getItemCount() {
            return QuestionManager.questions.get(categoryIndex).getQuestionPacks().size();
        }
    }
}
