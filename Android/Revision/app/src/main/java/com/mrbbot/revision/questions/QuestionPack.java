package com.mrbbot.revision.questions;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class QuestionPack implements Serializable {
    private String name, description;
    private ArrayList<Question> questions;
    private Categories category;

    public QuestionPack(String name, String description, ArrayList<Question> questions, Categories category) {
        this.name = name;
        this.description = description;
        this.questions = questions;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public Categories getCategory() {
        return category;
    }

    public void shuffle() {
        Log.d("Revision", "Shuffling...");
        Collections.shuffle(questions);
    }
}
