package com.mrbbot.revision.questions;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrbbot.revision.R;

class QuestionCard extends Question {
    private Object frontSide;
    private Object backSide;
    private boolean onFrontSide = true;

    QuestionCard(Object frontSide, Object backSide) {
        this.frontSide = frontSide;
        this.backSide = backSide;
    }

    private boolean isOnFrontSide() {
        return onFrontSide;
    }

    private void resetSide() {
        onFrontSide = true;
    }

    private void switchSide() {
        onFrontSide = !onFrontSide;
    }

    @Override
    protected View inflate(LayoutInflater inflater, ViewGroup container) {
        final View root = inflater.inflate(R.layout.question_type_card, container, false);

        final CardView cardView = (CardView) root.findViewById(R.id.question_type_card_card);
        final TextView textView = (TextView) root.findViewById(R.id.question_type_card_text);
        final ImageView imageView = (ImageView) root.findViewById(R.id.question_type_card_image);

        resetSide();
        setDetails(textView, imageView);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchSide();
                setDetails(textView, imageView);
            }
        });

        return root;
    }

    private void setDetails(TextView textView, ImageView imageView) {
        Object object = isOnFrontSide() ? frontSide : backSide;

        boolean text = true;
        if(object instanceof Spanned) {
            textView.setText((Spanned) object);
            text = true;
        } else if(object instanceof Drawable) {
            imageView.setImageDrawable((Drawable) object);
            text = false;
        } else if(object instanceof String) {
            Spanned spanned;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                spanned = Html.fromHtml((String) object, Html.FROM_HTML_MODE_COMPACT);
            } else {
                spanned = Html.fromHtml((String) object);
            }
            if(isOnFrontSide())
                frontSide = spanned;
            else
                backSide = spanned;
            textView.setText(spanned);
            text = true;
        } else if(object instanceof Integer) {
            Drawable drawable = ContextCompat.getDrawable(imageView.getContext(), (Integer) object);
            if(isOnFrontSide())
                frontSide = drawable;
            else
                backSide = drawable;
            imageView.setImageDrawable(drawable);
            text = false;
        }

        textView.setVisibility(text ? View.VISIBLE : View.GONE);
        imageView.setVisibility(!text ? View.VISIBLE : View.GONE);
    }

    @Override
    protected boolean checkAnswer(View root, Object... sentAnswers) {
        return true;
    }
}
