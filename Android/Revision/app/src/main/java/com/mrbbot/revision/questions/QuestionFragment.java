package com.mrbbot.revision.questions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class QuestionFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int categoryIndex = getArguments().getInt("category-index", -1);
        int packIndex = getArguments().getInt("pack-index", -1);
        int questionIndex = getArguments().getInt("question-index", -1);

        Question question = QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(packIndex).getQuestions().get(questionIndex);
        question.doInit();
        return question.inflate(inflater, container);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        int categoryIndex = getArguments().getInt("category-index", -1);
        int packIndex = getArguments().getInt("pack-index", -1);
        int questionIndex = getArguments().getInt("question-index", -1);

        if(categoryIndex != -1 && packIndex != -1 && questionIndex != -1) {
            QuestionManager.questions.get(categoryIndex).getQuestionPacks().get(packIndex).getQuestions().get(questionIndex).reset();
        }
    }
}
