package com.mrbbot.revision.questions;

import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mrbbot.revision.R;

class QuestionTickAll extends Question {
    private final String question;
    private final String[] answers;
    private final boolean[] correctAnswers;

    QuestionTickAll(String question, String[] answers, boolean[] correctAnswers) {
        this.question = question;
        this.answers = answers;
        this.correctAnswers = correctAnswers;
    }

    @Override
    protected View inflate(LayoutInflater inflater, final ViewGroup container) {
        final View root = inflater.inflate(R.layout.question_type_tick_all, container, false);

        TextView questionTextView = (TextView) root.findViewById(R.id.question_type_tick_all_question);
        questionTextView.setText(question);

        DisplayMetrics displayMetrics = inflater.getContext().getResources().getDisplayMetrics();
        int eightDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, displayMetrics);

        LinearLayout linearLayout = (LinearLayout) root.findViewById(R.id.question_type_tick_all_answers);
        for (String answer : answers) {
            CheckBox box = new CheckBox(inflater.getContext());
            box.setText(answer);
            box.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            box.setPadding(0, eightDp, 0, eightDp);
            linearLayout.addView(box);
        }

        final Button button = (Button) root.findViewById(R.id.question_type_tick_all_button);
        final Question questionObject = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionManager.nextQuestion(questionObject, container, root);
            }
        });

        return root;
    }

    @Override
    protected boolean checkAnswer(View root, Object... sentAnswers) {
        root.findViewById(R.id.question_type_tick_all_button).setEnabled(false);

        LinearLayout linearLayout = (LinearLayout) root.findViewById(R.id.question_type_tick_all_answers);

        boolean correct = true;

        for(int i = 0; i < answers.length; i++) {
            boolean correctAnswer = correctAnswers[i];

            CheckBox box = (CheckBox) linearLayout.getChildAt(i);
            box.setEnabled(false);
            boolean givenAnswer = box.isChecked();

            if(correctAnswer && givenAnswer)
                box.setTextColor(QuestionManager.COLOUR_CORRECT);
            if(correctAnswer != givenAnswer)
                box.setTextColor(QuestionManager.COLOUR_INCORRECT);

            if(givenAnswer != correctAnswer)
                correct = false;
        }

        return correct;
    }
}
