package com.mrbbot.revision.questions;

class QuestionTrueFalse extends QuestionMultipleChoice {
    QuestionTrueFalse(String question, boolean answer) {
        super(question, new String[] {"True", "False"}, answer ? 0 : 1);
    }
}
