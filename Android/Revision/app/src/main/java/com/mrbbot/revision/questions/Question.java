package com.mrbbot.revision.questions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class Question {
    private boolean initialized = false;
    final void doInit() {
        if(!initialized) {
            initialized = true;
            init();
        }
    }

    final void reset() {
        initialized = false;
    }

    protected void init() {}
    protected abstract View inflate(LayoutInflater inflater, final ViewGroup container);
    protected abstract boolean checkAnswer(View root, Object... sentAnswers);
}
