package com.mrbbot.revision.questions;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import java.io.Serializable;
import java.util.ArrayList;

public class Category implements Serializable {
    private String name;
    private ArrayList<QuestionPack> questionPacks;
    private int drawableID;
    private Categories category;

    Category(Categories category) {
        name = category.name;
        drawableID = category.drawableID;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public Drawable getDrawable(Context context) {
        return ContextCompat.getDrawable(context, drawableID);
    }

    void addQuestionPack(QuestionPack pack) {
        if(questionPacks == null)
            questionPacks = new ArrayList<>();
        questionPacks.add(pack);
    }

    public ArrayList<QuestionPack> getQuestionPacks() {
        if(questionPacks == null)
            questionPacks = new ArrayList<>();
        return questionPacks;
    }

    public Categories getCategory() {
        return category;
    }
}
