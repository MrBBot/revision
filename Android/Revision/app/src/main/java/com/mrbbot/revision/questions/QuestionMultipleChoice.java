package com.mrbbot.revision.questions;

import android.support.v7.widget.GridLayout;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mrbbot.revision.R;

class QuestionMultipleChoice extends Question {
    private final String question;
    private final String[] answers;
    private final int correctAnswer;

    QuestionMultipleChoice(String question, String[] answers, int correctAnswer) {
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
    }

    @Override
    protected View inflate(LayoutInflater inflater, final ViewGroup container) {
        final View root = inflater.inflate(R.layout.question_type_multiple_choice, container, false);

        TextView questionTextView = (TextView) root.findViewById(R.id.question_type_multiple_choice_question);
        questionTextView.setText(question);

        GridLayout answerGroup = (GridLayout) root.findViewById(R.id.question_type_multiple_choice_answers);
        int numberOfAnswers = answers.length;
        answerGroup.setRowCount((numberOfAnswers + 1) / 2);
        answerGroup.setColumnCount(2);
        DisplayMetrics displayMetrics = inflater.getContext().getResources().getDisplayMetrics();
        int eightDp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, displayMetrics);
        final Question questionObject = this;
        for(int i = 0; i < answers.length; i++) {
            RadioButton button = new RadioButton(inflater.getContext());
            button.setText(answers[i]);
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            button.setPadding(0, eightDp, 0, eightDp);
            GridLayout.LayoutParams params = new GridLayout.LayoutParams(GridLayout.spec(GridLayout.UNDEFINED, 1f), GridLayout.spec(GridLayout.UNDEFINED, 1f));
            answerGroup.addView(button, params);
            final int currentAnswerID = i;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuestionManager.nextQuestion(questionObject, container, root, currentAnswerID);
                }
            });
        }

        return root;
    }

    @Override
    protected boolean checkAnswer(View root, Object... sentAnswers) {
        GridLayout answerGroup = (GridLayout) root.findViewById(R.id.question_type_multiple_choice_answers);

        int givenAnswer = (int) sentAnswers[0];
        boolean correct = correctAnswer == givenAnswer;

        if(!correct) {
            RadioButton incorrectButton = (RadioButton) answerGroup.getChildAt(givenAnswer);
            incorrectButton.setTextColor(QuestionManager.COLOUR_INCORRECT);
        }

        RadioButton correctButton = (RadioButton) answerGroup.getChildAt(correctAnswer);
        correctButton.setTextColor(QuestionManager.COLOUR_CORRECT);

        for(int i = 0; i < answerGroup.getChildCount(); i++) {
            answerGroup.getChildAt(i).setEnabled(false);
        }

        return correct;
    }
}
